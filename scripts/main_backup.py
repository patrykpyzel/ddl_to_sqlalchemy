import pprint
import re
from mappings import DDL_TO_ALCHEMY

lines = [line.rstrip('\n').rstrip(',').lstrip('\t') for line in open('ddl.txt')]

alchemy = []

for line in lines:
    word_list = line.split()
    spaced_column_list = re.findall(r'"(.*?)"', line)
    last_word = word_list[-1].split('(')
    data_type = last_word[0]
    data_length = ''
    if len(last_word) > 1:
        data_length += last_word[1].rstrip(')')
    if spaced_column_list:
        spaced_column = spaced_column_list[0].lower()
        key = re.sub('[^0-9a-z_]+', '_', spaced_column)
        print('Column(\'{0}\', {1}({2}), key=\'{3}\'),'.format(spaced_column, DDL_TO_ALCHEMY[data_type], data_length, key))
    else:
        column = word_list[0].lower()
        print('Column(\'{0}\', {1}({2})),'.format(column, DDL_TO_ALCHEMY[data_type], data_length))
