DDL_TO_ALCHEMY = {
    'NVARCHAR2': 'NVARCHAR',
    'NUMBER': 'NUMBER',
    'BLOB': 'BLOB',
    'DATE': 'DateTime',
    'RAW': 'RAW',
    'VARCHAR2': 'VARCHAR',
    'CHAR': 'CHAR'
}

DDL_TO_ALCHEMY_OLD = {
    'NVARCHAR2': 'NVARCHAR',
    'NUMBER': 'NUMBER',
    'BLOB': 'BLOB',
    'DATE': 'DateTime',
    'RAW': 'RAW',
    'VARCHAR2': 'VARCHAR'
}